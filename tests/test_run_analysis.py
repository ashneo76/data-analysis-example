from report.run_analysis import *
import pytest


@pytest.fixture
def data():
    test_file = 'tests/resources/sample_logs.txt'
    return load_data(test_file)


def test_to_session_dict():
    session_dict = to_session_dict('{"url": "/url1", "timestamp": "2018-01-02T22:35:52.620579", "sessionId": "d2ef9462-888b-4af5-8bb4-636b812545c7", "experiments": "no-ads", "userId": 3}')

    for key in ['url', 'timestamp', 'sessionId', 'experiments', 'userId']:
        assert(key in session_dict.keys())


def test_load_data(data):
    assert(type(data) == list)
    assert(len(data) == 28)


def test_get_top_urls(data):
    # calculated independently:
    #   cat tests/resources/sample_logs.txt | cut -d' ' -f2 | sort | uniq -c | sort -r | awk '{print $2}'
    expected_top_urls = [
        "/url5",
        "/url2",
        "/url6",
        "/url1",
        "/url4",
        "/url3",
        "/url7",
        "/url8",
    ]

    actual_top_urls = get_top_urls(data)
    assert(expected_top_urls == actual_top_urls)


def test_get_session_timestamps(data):
    session_timestamps = get_session_timestamps(data)
    test_session_ts = session_timestamps['e026a936-54df-486a-b672-128f3f570fea']

    # Assert that we grouped by session id correctly
    # calculated independently:
    #   cat tests/resources/sample_logs.txt | cut -d' ' -f6 | sort | uniq -c | sort -r
    assert(len(test_session_ts) == 6)

    # Assert that the timestamps are in ascending order
    assert((test_session_ts[-1] - test_session_ts[0]).seconds > 0)


def test_calculate_session_times(data):
    session_times = calculate_session_times(data)

    # Assert that we have 8 observations that have more than 1 timestamp
    # calculated independently:
    #   cat tests/resources/sample_logs.txt | tr "\n" "," | sed -e "s|^|[|;s|$|]|"
    #       | jq "group_by(.sessionId) | map(select(length > 1)) | length"
    assert(len(session_times) == 8)

    # Assert that the session times are sorted in an ascending order
    assert(session_times[0] < session_times[-1])


def test_get_median():
    example_data = [
        [4, 5, 6, 7, 89],
        [56, 89, 12]
    ]
    expected_medians = [
        6,
        56
    ]

    for index, test_data in enumerate(example_data):
        assert(expected_medians[index] == get_median(test_data))


def test_get_mean():
    example_data = [
        [4, 5, 6, 7, 89],
        [56, 89, 12]
    ]
    expected_means = [
        22.2,
        52.333333333333336
    ]

    for index, test_data in enumerate(example_data):
        assert(expected_means[index] == get_mean(test_data))
