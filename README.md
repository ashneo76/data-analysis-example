Steps on running the analysis:

* In a suitable python virtual environment or anaconda environment, install dependencies:

    `pip install -r requirements.txt`

* Run the analyser on a log file, called user_log.txt, like so:

    `python report/run_analysis.py -f user_log.txt`

* Run the tests:

    `python -m pytest`
