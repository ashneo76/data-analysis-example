import json
import arrow
from tqdm import tqdm
import math
import argparse


def to_session_dict(log_str: str) -> dict:
    """
    This method takes a log string and converts it to a python dictionary
    :param log_str: A log string from the input file
    :return: A dictionary representing the input data
    """
    session_dict = json.loads(log_str.strip())
    session_dict['timestamp'] = arrow.get(session_dict['timestamp'])

    return session_dict


def load_data(filename: str) -> list:
    """
    This method takes in a filename, loads all the lines and returns a list of dictionaries
    representing the records.
    :param filename: The name of the log file
    :return: The list of dictionary object recording a log entry.
    """
    print("Parsing log file: {}".format(filename))
    with open(filename, "r") as f:
        data = [to_session_dict(line) for line in tqdm(f.readlines())]

    return data


def get_top_urls(data: list) -> list:
    """
    This method calculates the visits for each url and ranks the urls by visits
    :param data: The list of parsed log messages
    :return: A ranked list of urls in descending order
    """
    print("Calculating top urls...")

    url_counts = {}

    # Calculate visits per url
    for log in data:
        if log['url'] in url_counts.keys():
            url_counts[log['url']] += 1
        else:
            url_counts[log['url']] = 1

    # Encode visit count and url as a sortable string
    url_combos = ["{}-{}".format(url_counts[key], key) for key in url_counts.keys()]
    url_combos.sort(reverse=True)
    # Decode top urls from sorted list
    top_urls = [item.split('-')[1] for item in url_combos]

    return top_urls


def get_session_timestamps(data: list) -> dict:
    """
    This method takes in the list of parsed log messages and groups them by session id.
    :param data: The list of parsed log messages
    :return: The timestamps grouped by each session id and sorted in ascending order
    """
    sessions = {}
    for log in data:
        if log['sessionId'] in sessions.keys():
            sessions[log['sessionId']].append(log['timestamp'])
        else:
            sessions[log['sessionId']] = [log['timestamp']]

    # Sort the timestamps in ascending order
    for session in sessions.keys():
        sessions[session].sort()

    return sessions


def calculate_session_times(data):
    print("Calculating session times...")
    session_times = []

    sessions = get_session_timestamps(data)
    for session in sessions.keys():
        if len(sessions[session]) > 1:
            # we ignore sessions with no end times.
            max_time = sessions[session][-1]
            min_time = sessions[session][0]
            session_times.append(int((max_time - min_time).total_seconds() * 1000))

    # Sort session times in ascending order
    session_times.sort()

    return session_times


def get_median(observation_list: list):
    observation_list.sort()
    return observation_list[int(math.floor(len(observation_list) / 2))]


def get_mean(observation_list: list):
    return float(sum(observation_list) / len(observation_list))


def get_analysis_results(data: list):
    top_urls = get_top_urls(data)
    session_times = calculate_session_times(data)
    results = {
        "most_viewed_urls": top_urls[:5],
        "session_stats": {
            "median": get_median(session_times),
            "mean": get_mean(session_times),
            "max": session_times[-1],
            "min": session_times[0]
        }
    }

    return results


def main():
    parser = argparse.ArgumentParser("run_analysis.py")
    parser.add_argument("-f", "--file", help="User log file", type=str, default="user_log.txt")
    args = parser.parse_args()

    filename = args.file
    data = load_data(filename)
    results = get_analysis_results(data)
    print("Analysis results:")
    print(json.dumps(results, indent=4, sort_keys=True))


if __name__ == '__main__':
    main()
